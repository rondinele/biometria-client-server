/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infoway.biometria.client.util;

import com.griaule.grfingerjava.Template;
import java.awt.image.RenderedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author humberto
 */
public class Utils {

    public static byte[] converter(RenderedImage renderedImage) {
        byte[] imageInByte = null;

        try {

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(renderedImage, "png", baos);
            baos.flush();
            imageInByte = baos.toByteArray();
            baos.close();

        } catch (IOException e) {

        }

        return imageInByte;
    }

    public static String qualidade(int qualidade) {
        String descricaoQualidade;

        switch (qualidade) {
            case Template.HIGH_QUALITY:
                descricaoQualidade = "Qualidade Alta";
                break;
            case Template.MEDIUM_QUALITY:
                descricaoQualidade = "Qualidade Media";
                break;
            case Template.LOW_QUALITY:
                descricaoQualidade = "Qualidade Baixa";
                break;
            case Template.UNKNOWN_QUALITY:
            default:
                descricaoQualidade = "Qualidade desconhecida";
                break;
        }

        return descricaoQualidade;
    }

    public static String byteArrayToString(byte[] data) {
        StringBuilder sb = new StringBuilder();
        for(byte b: data){
            sb.append(b);
            sb.append(";");
        }
        return sb.toString();
    }

}
