package br.com.infoway.biometria.client.configuration;

import br.com.infoway.biometria.client.util.GriauleInstaller;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Application extends SpringBootServletInitializer {

    public static void iniciar(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    public static void instalar(String[] args) {
        if (args != null && args.length > 0) {
            String licenca = args[0];

            try {
                GriauleInstaller.install(licenca);
            } catch (IOException ex) {
                Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
