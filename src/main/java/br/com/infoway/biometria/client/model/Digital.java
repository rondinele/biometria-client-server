/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infoway.biometria.client.model;

import org.springframework.stereotype.Component;

/**
 *
 * @author humberto
 */
@Component
public final class Digital {

    private Object identificador;

    private byte[] imagem;

    private String conteudo;

    private String qualidade;

    private String mensagem;

    private String acao;

    private int nivel;

    public Digital() {

    }

    public Object getIdentificador() {
        return identificador;
    }

    public void setIdentificador(Object identificador) {
        this.identificador = identificador;
    }

    public void setImagem(byte[] imagem) {
        this.imagem = imagem;
    }

    public byte[] getImagem() {
        return imagem;
    }

    public void setQualidade(String qualidade) {
        this.qualidade = qualidade;
    }

    public String getQualidade() {
        return qualidade;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }

    public String getAcao() {
        return acao;
    }

    public void setAcao(String acao) {
        this.acao = acao;
    }

    public String getConteudo() {
        return conteudo;
    }

    public void setConteudo(String conteudo) {
        this.conteudo = conteudo;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

}
