/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.infoway.biometria.client.controller;

import br.com.infoway.biometria.client.model.Digital;
import br.com.infoway.biometria.client.util.GriauleInstaller;
import br.com.infoway.biometria.client.util.Utils;
import com.griaule.grfingerjava.FingerprintImage;
import com.griaule.grfingerjava.GrFingerJava;
import com.griaule.grfingerjava.GrFingerJavaException;
import com.griaule.grfingerjava.IFingerEventListener;
import com.griaule.grfingerjava.IImageEventListener;
import com.griaule.grfingerjava.IStatusEventListener;
import com.griaule.grfingerjava.MatchingContext;
import com.griaule.grfingerjava.Template;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.ImageIO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author humberto
 */
@Controller
@Configuration
public class GriaulerController implements IStatusEventListener, IImageEventListener,
        IFingerEventListener {

    private final Logger logger = LoggerFactory.getLogger(GriaulerController.class);

    private final String BASE_URL = "biometria.infoway/";

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    private Digital digital;

    @MessageMapping("/teste")
    @SendTo("/contexto/digital")
    public Digital teste() throws Exception {
        try {
            digital.setMensagem("Teste de serviço");
            digital.setQualidade("Descrição da qualidade");
            InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream("imagens/fingerprint-icon.png");
            digital.setImagem(Utils.converter(ImageIO.read(resourceAsStream)));
        } catch (IOException ex) {
            logger.error(":-(", ex);

        }
        return digital;
    }

    @MessageMapping("/leitor")
    @SendTo("/contexto/digital")
    public Digital leitor() throws Exception {
        logger.info("####APP####Apresentando imagem default.");
        try {
            digital.setMensagem("Teste de serviço");
            digital.setQualidade("Descrição da qualidade");
            InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream("imagens/fingerprint-icon.png");
            digital.setImagem(Utils.converter(ImageIO.read(resourceAsStream)));
        } catch (IOException ex) {
            logger.error(":-(", ex);

        }
        return digital;
    }

    @MessageMapping("/info")
    public Map info() {
        Map<String, String> info = new HashMap();
        try {
            String license = (GrFingerJava.getLicenseType() == GrFingerJava.GRFINGER_JAVA_FULL ? "Identification" : "Verification");
            String version = GrFingerJava.getMajorVersion() + "." + GrFingerJava.getMinorVersion();

            info.put("software", "Fingerprint SDK");
            info.put("version", version);
            info.put("license", license);

        } catch (GrFingerJavaException ex) {
            logger.error("Informações", ex);
        }
        return info;
    }

    @MessageMapping("/liga")
    public void liga() {
        logger.info("####APP####Ligando leitor.");
        try {
            logger.info("Registrando biblioteca nativa.");
            File destDir = GriauleInstaller.getFinalDestDir();
            GrFingerJava.setNativeLibrariesDirectory(destDir);
            logger.info("Registrando local da lincença.");
            GrFingerJava.setLicenseDirectory(destDir);
            logger.info("Iniciando captura da digital.");
            GrFingerJava.initializeCapture(this);
        } catch (NullPointerException ex) {
            logger.error("Informações", ex);
        } catch (GrFingerJavaException ex) {
            logger.error("Informações", ex);
        } catch (RuntimeException ex) {
            logger.error("Informações", ex);
        } catch (UnsatisfiedLinkError e) {
            logger.error("Informações", e);
        }

    }

    @MessageMapping("/envia")
    @SendTo("/contexto/desliga")
    public void envia() {
        logger.info("####APP####Enviando imagem.");
        digital.setIdentificador("");

        try {
            GrFingerJava.finalizeCapture();
        } catch (GrFingerJavaException ex) {
            logger.error("Erro a finalizar a captura", ex);
        }
    }

    @Override
    public void onSensorPlug(String idSensor) {
        logger.info("####APP####Leitor conectado.");
        try {
            GrFingerJava.startCapture(idSensor, this, this);
        } catch (GrFingerJavaException ex) {
            logger.error(idSensor, ex);
        }
    }

    @Override
    public void onSensorUnplug(String idSensor) {
        logger.info("####APP####Leitor desconectado.");
        try {
            GrFingerJava.stopCapture(idSensor);
        } catch (GrFingerJavaException ex) {
            logger.error(idSensor, ex);
        }
    }

    @Override
    public void onImageAcquired(String idSensor, FingerprintImage fingerprint) {
        logger.info("####APP####Lendo Imagem");
        try {
            MatchingContext matchingContext = new MatchingContext();
            Template extract = matchingContext.extract(fingerprint);
            
            int quality = extract.getQuality();
            byte[] data = extract.getData();

            digital.setImagem(Utils.converter(fingerprint));
            digital.setConteudo(Utils.byteArrayToString(data));
            digital.setQualidade(Utils.qualidade(quality));
            digital.setNivel(quality);

            this.messagingTemplate.convertAndSend("/contexto/digital", digital);
        } catch (GrFingerJavaException ex) {
            logger.error(idSensor, ex);
        }
    }

    @Override
    public void onFingerDown(String idSensor) {
        logger.info("####APP####Aprensentou ao leitor");
    }

    @Override
    public void onFingerUp(String idSensor) {
        logger.info("####APP####Retirou do leitor");
    }

}
