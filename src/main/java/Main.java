/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import br.com.infoway.biometria.client.configuration.Application;

/**
 *
 * @author humberto
 */
public class Main {

    public static void main(String[] args) {
        if (args == null || args.length == 0) {
            Application.iniciar(args);
        } else {
            Application.instalar(args);
        }
    }

}
