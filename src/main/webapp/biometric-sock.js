  
var stompClient = null;
var el1, el2, el3, el4 = null;

	var conecta = function(elementsDigital, elementsImageDigital, elementsQualityDigital, elementsQualityDigitalMsg) {
		el1 = elementsDigital;
		el2 = elementsImageDigital;
		el3 = elementsQualityDigital;
		el4 = elementsQualityDigitalMsg;
        var socket = new SockJS('http://localhost:8080/biometria/');
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function (frame) {

            stompClient.subscribe('/contexto/digital', function (contexto) {
                mostrarDigital(JSON.parse(contexto.body), el1, el2, el3, el4);
            });
            stompClient.subscribe('/contexto/desliga', function () {
                desconectar();
            });

            stompClient.send("/app/liga", {}, {});
        });
    }

	var mostrarDigital = function(contexto, elementsDigital, elementsImageDigital, elementsQualityDigital, elementsQualityDigitalMsg) {
		if (elementsDigital) {
			jQuery('#' + elementsDigital[0]).val(contexto.conteudo);
		}
		
		if (elementsImageDigital) {
			jQuery('#' + elementsImageDigital[0]).attr('src', 'data:image/png;base64,' + contexto.imagem);
		}
		
		if (elementsQualityDigital) {
			jQuery('#' + elementsQualityDigital[0]).val(contexto.nivel);
		}
		
		if (elementsQualityDigitalMsg) {
			jQuery('#' + elementsQualityDigitalMsg[0]).text(contexto.qualidade);
		}
	}

	var envia = function() {
        stompClient.send("/app/envia", {}, {});
    }

	var desconectar = function() {
        if (stompClient !== null) {
            stompClient.disconnect();
        }
    }
